$(document).ready(function() {
    var $jours = $('.jour');
    var $puces = $('.bullets .entypo-record');

    function init() {
        $('body').addClass('isok');
        $jours.hide();
        $('.wrapper').fadeIn('slow', function() {
            $jours.first().fadeIn('slow');
            $puces.removeClass('active');
            $puces.first().addClass('active');
        });

    }
    $puces.click(function() {
        var $this = $(this);
        var cible = $this.attr('data-cible');
        $jours.hide();

        $($jours.get(cible)).fadeIn()
        $puces.removeClass('active');
        $this.addClass('active');
    });
    init();
});